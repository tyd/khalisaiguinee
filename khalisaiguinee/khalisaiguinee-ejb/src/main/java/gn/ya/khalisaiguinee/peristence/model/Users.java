package gn.ya.khalisaiguinee.peristence.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import gn.ya.khalisaiguinee.peristence.Constants;
import gn.ya.khalisaiguinee.peristence.model.enums.AgenciesEnum;

/**
 * 
 * @author ydiallo
 *
 */
@Entity
@Table(name = Constants.Tables.USERS, schema = Constants.SCHEMA)
public class Users extends AbstractEntity {

	/** UID.  **/
	private static final long serialVersionUID = 1L;
	
	@Embedded
	private Name name;
	
	@Column(name = "AGENCY", nullable = false)
	@Enumerated(EnumType.STRING)
	private AgenciesEnum agency;
	
	@Embedded
	private Adresse adresse;
	
	
	
	

}
