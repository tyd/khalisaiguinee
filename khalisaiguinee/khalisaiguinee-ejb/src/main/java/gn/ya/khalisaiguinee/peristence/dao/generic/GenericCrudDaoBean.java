package gn.ya.khalisaiguinee.peristence.dao.generic;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

/**
 * 
 * @author ydiallo
 *
 */
@Stateless
public class GenericCrudDaoBean extends AbstractGenericDaoBean implements GenericCrudDao {

	@EJB
	private EntityManagerProvider emProvider;
	
	@Override
	protected EntityManager getEm() {
		return emProvider.getEm();
	}

}
