package gn.ya.khalisaiguinee.peristence.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author ydiallo
 *
 */
@Embeddable
public class Name {

	@NotNull
	@Column(name = "FIRST_NAME", length = 150, nullable = false)
	private String firstName;
	
	@NotNull
	@Column(name = "LAST_NAME", length = 250, nullable = false)
	private String lastName;
	
	@Column(name = "PHONE", nullable = true)
	private String phone;
	
	@Column(name = "EMAIL", nullable = false)
	private String email;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Name [firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone + ", Email=" + email +"]";
	}
	
}
