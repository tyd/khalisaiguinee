package gn.ya.khalisaiguinee.peristence.model.enums;


/**
 * @author ydiallo
 *
 */
public enum AgenciesEnum {

	BRUXELLES("BRUXELLES"),
	CHARLEROI("CHARLEROI"),
	MADINA("MADINA"),
	COSA("COSA");
	
	
	private String code;
	
	private AgenciesEnum(String inCode) {
		this.code = inCode;
	}
	
	public static AgenciesEnum getValueFor(String code) {
		for (AgenciesEnum enu : AgenciesEnum.values()) {
			if (enu.getCode().equals(code)) {
				return enu;
			}
		}
		return null;
	}

	public String getCode() {
		return code;
	}

	
	
}
