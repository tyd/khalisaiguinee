package gn.ya.khalisaiguinee.action;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author ydiallo
 *
 */
@Named
@SessionScoped
public class SendMoneyAction implements Serializable{

	/** UID.*/
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SendMoneyAction.class);
	/*
	 * @Inject private MoneyTransferMgr transferMgrBean;
	 * 
	 * @Inject private SendMoneyForm data;
	 */
	
	private  FacesMessage message ;
	
	public void init() {
		
	}
	
	//public void sendMoney() {
	//	LOGGER.info("sendMoney-begin");
		
		/*try {
			transferMgrBean.makeTransfer(getTransfer(), getUser());
		} catch (ServiceException e) {
			LOGGER.error("Error occur while make an transfer: error={}", e);
			//message.setSeverity(FacesMessage.SEVERITY_ERROR);
		}
		
		LOGGER.info("sendMoney-end");
	}*/
	
	/**
	 * to be removed when implement security.
	 * @return connected user
	 */
	/*private UsersDto getUser() {
		UsersDto user = new UsersDto();
		
		user.setFristName("Diallo");
		user.setLastName("THierno yaya");
		user.setAgency(AgencyEnum.KINDIA);
		user.setPhone("0481281985");
		
		return user;
	}
	
	private TransferDto getTransfer() {
		TransferDto tsf = new TransferDto();
		tsf.setAgency(data.getDestination());
		tsf.setAmount(data.getAmount());
		tsf.setFristName(data.getFristName());
		tsf.setLastName(data.getLastName());
		tsf.setPhone(data.getPhone());
		
		return tsf;
	}*/
	
	
}
