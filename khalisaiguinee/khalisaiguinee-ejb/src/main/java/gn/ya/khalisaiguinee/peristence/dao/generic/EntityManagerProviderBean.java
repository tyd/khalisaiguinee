package gn.ya.khalisaiguinee.peristence.dao.generic;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * @author ydiallo
 *
 */
@Stateless
public class EntityManagerProviderBean implements EntityManagerProvider {
	
	@PersistenceContext(unitName = "khalisaiguinee")
	private EntityManager em;

	@Override
	public EntityManager getEm() {
		System.out.println("name em==" + em);
		return em;
	}

}
