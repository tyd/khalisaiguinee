package gn.ya.khalisaiguinee.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("selectItemConverter")
public class SelectItemConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(value == null)
			return null;
		return new String[] {value};
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value ==null)
			return null;
		return value.toString();
	}

	
}
