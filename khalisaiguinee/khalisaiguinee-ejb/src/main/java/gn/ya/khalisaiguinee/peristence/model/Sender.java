package gn.ya.khalisaiguinee.peristence.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * 
 * @author ydiallo
 *
 */
@Entity
@DiscriminatorValue("SENDER")
public class Sender extends Operations {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "FEE", nullable = true)
	private BigDecimal fee;

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}
	
	

}
