package gn.ya.khalisaiguinee.peristence.dao.generic;

import javax.persistence.EntityManager;

public interface EntityManagerProvider {
	
	EntityManager getEm();

}
