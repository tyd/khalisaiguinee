package gn.ya.khalisaiguinee.data;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;


@Named
@RequestScoped
public class SendMoneyForm implements Serializable{

	/** UID. */
	private static final long serialVersionUID = 1L;
	
	//private AgencyEnum destination;
	
	private BigDecimal amount;
	
	private String fristName;
	
	private String lastName;
	
	private String phone;

	/*
	 * public AgencyEnum getDestination() { return destination; }
	 * 
	 * public void setDestination(AgencyEnum destination) { this.destination =
	 * destination; }
	 */

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
