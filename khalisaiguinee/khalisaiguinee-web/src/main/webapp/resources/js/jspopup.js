/**
 *  
 *
 * Generate popup that looks like alert, confirm and prompt javascript popup.
 * Warning : the css property "overflow" of the page must be set to "auto".
 *
 * TODO
 * Doc for CSS
 * In prompt popup under firefox, the cursor in the input is not visible
 * Better window resizing (not with hidding et re-showing popup)
 * Problem if the windows is too small
 */
var closePic = 'images/cross.gif'; // Close picture (cross in the top right of the popup)
var jsPopupWidth = 400;

/**
 * Encode String in html form
 *
 * @param the string to encode
 * @return the encoded string
 */
function encodeHtml(str) {
var div = document.createElement("div");
var txt = document.createTextNode(str);
div.appendChild(txt);
return div.innerHTML;
}

/**
 * Get the window size.
 *
 * @return an array [x, y] with the window content size, in pixel
 */
function getWindowSize() {
var width = 0;
var height = 0;
if (window.innerHeight) {
width = window.innerWidth;
height = window.innerHeight;
} else {
width = document.body.clientWidth;
height = document.body.clientHeight;
}
//alert( 'The window width is ' + width + ' and the window height is ' + height );
return [width, height];
}

/**
 * Get the page size
 *
 * @return an array [x, y] with the full page size (including scroll)
 */
function getPageSizeWithScroll(){
var yWithScroll = 0;
var xWithScroll = 0;
if (window.innerHeight && window.scrollMaxY) {// Firefox
yWithScroll = window.innerHeight + window.scrollMaxY;
xWithScroll = window.innerWidth + window.scrollMaxX;
} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
yWithScroll = document.body.scrollHeight;
xWithScroll = document.body.scrollWidth;
} else { // works in Explorer 6 Strict, Mozilla (not FF) and Safari
yWithScroll = document.body.offsetHeight - 4;
xWithScroll = document.body.offsetWidth - 4;
  }
arrayPageSizeWithScroll = new Array(xWithScroll,yWithScroll);
//alert( 'The page width is ' + xWithScroll + ' and the page height is ' + yWithScroll );
return arrayPageSizeWithScroll;
}

/**
 * Get the scroll x and y
 *
 * @return an array [x, y] with the scroll values
 */
function getScrollXY() {
var scrOfX = 0, scrOfY = 0;
if (window.pageYOffset) {
//Netscape compliant
scrOfY = window.pageYOffset;
scrOfX = window.pageXOffset;
} else if( document.body && (document.body.scrollLeft || document.body.scrollTop)) {
   //DOM compliant
   scrOfY = document.body.scrollTop;
   scrOfX = document.body.scrollLeft;
} else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
   //IE6 standards compliant mode
   scrOfY = document.documentElement.scrollTop;
   scrOfX = document.documentElement.scrollLeft;
}
//alert('Scroll X : ' + scrOfX + ', scroll Y : ' + scrOfY);
  return [ scrOfX, scrOfY ];
}



/**
 * Add an attribute to a DOM element
 * @param tag the DOM element
 * @param name the name of the attibute
 * @param value the value of the attribute
 */
function addAttribute(tag, name, value) {
    var attribute = document.createAttribute(name);
    attribute.nodeValue = value;
    tag.setAttributeNode(attribute);
}

/**
 * True if a popup is visible. Only one popup is visible in the same time
 */
var jsPopupVisible = false;



/**
 * Destroy a popup container
 */
function jsHidePopup() {
var popup = document.getElementById('jsPopup');
if (popup != null) {
popup.parentNode.removeChild(popup);
popup = document.getElementById('jsPopupBackground');
if (popup != null) {
popup.parentNode.removeChild(popup);
}
}
jsPopupVisible = false;
window.onresize = function(){};
}


/**
 * Create a container for a popup. This container contains a header with a title and a close cross,
 * and a body with the text.
 *
 * @param text the text in the popup. In HTML format and can use html tag. (default is "")
 * @param title the title in the popup. In HTML format and can use html tag. (default is "")
 */
function jsShowPopup(text, title, canClose) {
if (canClose == null || canClose != false) {
canClose = true;
}
var pageSizes = getPageSizeWithScroll();
var scrolls = getScrollXY();
var windowSizes = getWindowSize();
if (windowSizes[1] > pageSizes[1]) {
pageSizes[1] = windowSizes[1];
}
if (windowSizes[0] > pageSizes[0]) {
pageSizes[0] = windowSizes[0];
}
if (windowSizes[1] < pageSizes[1] && window.pageYOffset !== undefined) {
pageSizes[0] -= 16;
}

var body = document.body;
if (body != null) {
// Background creation
var background = document.createElement('iframe');
addAttribute(background, 'id', 'jsPopupBackground');
addAttribute(background, 'class', 'jsPopupBackground');
body.appendChild(background);
background.style.display = 'block';
background.style.position = 'absolute';
background.style.left = '0px';
background.style.top = '0px';
background.style.margin = '0px';
background.style.zIndex = '995';
background.style.width = pageSizes[0] + 'px';
background.style.height = pageSizes[1] + 'px';
background.style.border = 'none';
// Popup creation
var mainDiv = document.createElement('div');
addAttribute(mainDiv, 'id', 'jsPopup');
addAttribute(mainDiv, 'class', 'jsPopup');
body.appendChild(mainDiv);
var popup = document.getElementById('jsPopup');
var html = '<table class="jsPopupContainer" id="jsPopupContainer" >';
html += '<thead class="jsPopupContainerHead"><tr><th><table width="100%"><tr><td>';
if (title != null) {
html += title;
} else {
html += '&nbsp;';
}
if (canClose) {
html += '</td><td width="1"><input type="image" onclick="jsHidePopup()" src="' + closePic + '" width="15" height="13" /></td></tr></table></th>';
} else {
html += '</td><td width="1">&nbsp;</td></tr></table></th>';
}
html += '</tr></thead>';
html += '<tbody class="jsPopupContainerBody"><tr>';
if (text != null) {
            var regexp = new RegExp('\n', 'g');
text = text.replace(regexp,'<br />');
html += '<td>' + text + '</td>';
} else {
html += '<td>&nbsp;</td>';
}
html += '</tr></tbody>';
html += '</table>';
popup.innerHTML = html;
var inPopup = document.getElementById('jsPopupContainer');
inPopup.style.width = jsPopupWidth + 'px';
inPopup.style.left = (scrolls[0] + ((windowSizes[0]/2)-200)) + 'px';
inPopup.style.top = (scrolls[1] + (windowSizes[1]/3)) + 'px';
inPopup.style.position = 'absolute';
inPopup.style.zIndex = '996';
document.getElementById('jsPopup').focus();
jsPopupVisible = true;
window.onresize = function() {
jsHidePopup();
jsShowPopup(text, title);
};
}
}






/**
 * Create an Alert popup if no popup exist, destroy the popup else.
 *
 * @param text the alert message. In HTML format and can use html tag. (default is "")
 * @param title the alert title. In HTML format and can use html tag. (default is null)
 * @param okBtnTxt the text of the "ok" button. In HTML format and can use html tag. (default is "Ok")
 */
function jsAlert(text, title, okBtnTxt) {
if (!jsPopupVisible) {
// New popup
if (okBtnTxt == null) {
okBtnTxt = "Ok";
}
if (text == null) {
text = "";
}
text += '<br /><input type="button" value="' + okBtnTxt + '" onclick="jsHidePopup()" id="jsPopupAlert"/>';
jsShowPopup(text, title);
document.getElementById('jsPopupAlert').focus();
return false;
} else {
// Popup already running, remove it and do action
jsHidePopup();
}

}


/**
 * Value returned by a Confirm popup
 */
var jsPopupConfirm = false;

/**
 * Create a Confirm popup if no popup exist, destroy the popup else.
 *
 * @param fctTrue the javascript code called when "ok" button is pressed (in text mode)
 * @param fctFalse the javascript code called when "cancel" button is pressed (in text mode)
 * @param text the confirm message. In HTML format and can use html tag. (default is "")
 * @param title the confirm title. In HTML format and can use html tag. (default is null)
 * @param trueBtnTxt the text of the "ok" button. In HTML format and can use html tag. (default is "Ok")
 * @param falseBtnTxt the text of the "cancel" button. In HTML format and can use html tag. (default is "Cancel")
 * @return the value of var jsPopupConfirm (set to true when clicking on "ok" button, set to false when clicking on "cancel" button)
 */
function jsConfirm(fctTrue, fctFalse, text, title, trueBtnTxt, falseBtnTxt) {
if (!jsPopupVisible) {
// New popup
jsPopupConfirm = false;
if (trueBtnTxt == null) {
trueBtnTxt = "Ok";
}
if (falseBtnTxt == null) {
falseBtnTxt = "Cancel";
}
if (text == null) {
text = "";
}
text += '<br /><input type="button" value="' + trueBtnTxt + '" onclick="jsPopupConfirm=true;' + fctTrue + '; jsHidePopup()" id="jsConfirmTrue"/> <input type="button" value="' + falseBtnTxt + '" onclick="jsPopupConfirm=false;' + fctFalse + ';jsHidePopup()" id="jsConfirmFalse" />';
jsShowPopup(text, title);
document.getElementById('jsConfirmFalse').focus();
return false;
} else {
// Popup already running, remove it and do action
jsHidePopup();
return jsPopupConfirm;
}
}



/**
 * Value returned by a Prompt popup
 */
var jsPopupPrompt = "";

/**
 * Create a prompt popup if no popup exist, destroy the popup else.
 *
 * @param fctReturn the javascript code called when "ok" button is pressed (in text mode)
 * @param fctCancel the javascript code called when "cancel" button is pressed (in text mode)
 * @param defaultValue the default value for the promp input textbox
 * @param text the prompt message. In HTML format and can use html tag. (default is "")
 * @param title the prompt title. In HTML format and can use html tag. (default is null)
 * @param okBtnTxt the text of the "ok" button. In HTML format and can use html tag. (default is "Ok")
 * @param canceleBtnTxt the text of the "cancel" button. In HTML format and can use html tag. (default is "Cancel")
 * @return the value of var jsPopupPrompt (set to the value of the promp input textbox when clicking on "ok" button, set to "" when clicking on "cancel" button)
 */
function jsPrompt(fctReturn, fctCancel, defaultValue, text, title, okBtnTxt, cancelBtnTxt) {
if (!jsPopupVisible) {
// New popup
jsPopupPrompt = "";
if (okBtnTxt == null) {
okBtnTxt = "Ok";
}
if (cancelBtnTxt == null) {
cancelBtnTxt = "Cancel";
}
if (defaultValue == null) {
defaultValue = "";
}
if (text == null) {
text = "";
}
text += '<br /><input type="text" class="JsPopupPromptValue" id="JsPopupPromptValue" value="' + defaultValue + '" style="width: 100%"/>';
text += '<br /><input type="button" value="' + okBtnTxt + '" onclick="jsPopupPrompt=document.getElementById(\'JsPopupPromptValue\').value;' + fctReturn + '; jsHidePopup()" id="jsPromptRetrun"/> <input type="button" value="' + cancelBtnTxt + '" onclick="' + fctCancel + '; jsHidePopup()" id="jsPromptCancel" />';
jsShowPopup(text, title);
document.getElementById('JsPopupPromptValue').focus();
document.getElementById('JsPopupPromptValue').select();
return false;
} else {
// Popup already running, remove it and do action
jsHidePopup();
return jsPopupPrompt;
}
}

function jsConfirmClickClick(buttonID) {
var btn = document.getElementById(buttonID);
if (btn != null) {
try {
btn.click();
} catch (e) {
btn.onclick();
}
}
}
/**
 * Confirm popup when clicking on a button. This method must be used in "onclick" attribute.
 *
 * @param buttonID The button id
 * @param text the confirm message. In HTML format and can use html tag. (default is "")
 * @param title the confirm title. In HTML format and can use html tag. (default is null)
 * @param trueBtnTxt the text of the "ok" button. In HTML format and can use html tag. (default is "Ok")
 * @param falseBtnTxt the text of the "cancel" button. In HTML format and can use html tag. (default is "Cancel")
 * @return the value of var jsPopupConfirm (set to true when clicking on "ok" button, set to false when clicking on "cancel" button)
 */
function jsConfirmClick(buttonID, text, title, okBtnTxt, cancelBtnTxt) {
var fct = 'jsConfirmClickClick(\'' + buttonID + '\')';
return jsConfirm(fct, "", text, title, okBtnTxt, cancelBtnTxt);
}

/**
 * Show the "in progress" A4J box
 */
function showInProgress() {
var box = document.getElementById('_viewRoot:status.start');
if (box != null) {
box.style.display = 'block';
}
return true;
}

/**
 * Set the focus
 */
function setFocus(id) {
var elem = document.getElementById(id);
if (elem != null) {
try {
elem.focus();
if ((elem.type == 'text' || elem.tagName == 'textarea' || elem.tagName == 'TEXTAREA') && elem.value != '') {
elem.select();
}
} catch (err) {}
}
}