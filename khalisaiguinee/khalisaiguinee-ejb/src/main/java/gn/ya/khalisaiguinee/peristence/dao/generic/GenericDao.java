package gn.ya.khalisaiguinee.peristence.dao.generic;

public interface GenericDao {
	
	<T extends Object> void create(T entity);
}
