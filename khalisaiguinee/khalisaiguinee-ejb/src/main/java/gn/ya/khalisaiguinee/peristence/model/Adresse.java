package gn.ya.khalisaiguinee.peristence.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * 
 * @author ydiallo
 *
 */
@Embeddable
public class Adresse {

	@Column(name = "COUNTRY", nullable = false)
	private String country;
	
	@Column(name = "TOWN", nullable = false)
	private String town;
	
	@Column(name = "ZIP_CODE", nullable = true)
	private String zipCode;
	
	@Column(name = "STREET_NAME", nullable = true)
	private String streetName;
	
	@Column(name = "STREET_NUMBER", nullable = true)
	private String streetNumber;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	@Override
	public String toString() {
		return "Adresse [country=" + country
			             + ", town=" + town 
			             + ", zipCode=" + zipCode
			             + ", streetName=" + streetName
			             + ", streetNumber=" + streetNumber +
			           "]";
	}
	
	
}
