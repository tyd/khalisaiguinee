$(document).ready(function() {
var cookie = readCookie('showMenu');
if (cookie != null && cookie == 'hidden'){
// menu is hidden
document.getElementById('menuform:menuPanel').style.display = 'none';
        document.getElementById('menuform:menuButtonShow').style.display = 'block';
        document.getElementById('menuform:menuButtonHide').style.display = 'none';
        $('#pageContent').removeClass('visibleMenu').addClass('hiddenMenu');
} else {
// menu is visible
document.getElementById('menuform:menuPanel').style.display = 'block';
        document.getElementById('menuform:menuButtonShow').style.display = 'none';
        document.getElementById('menuform:menuButtonHide').style.display = 'block';
        $('#pageContent').removeClass('hiddenMenu').addClass('visibleMenu');
}
$('h3.ui-panelmenu-header > span.ui-icon-triangle-1-s, h3.ui-panelmenu-header > span.ui-icon-triangle-1-e').hover(
function () {
$(this).parent().addClass('ui-state-hover');
},
function () {
$(this).parent().removeClass("ui-state-hover");
}
);
});


function createCookie(name, value, days) {
    var d = new Date();
    d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return "";
}

function eraseCookie(name) {
createCookie(name, '', -1);
}

