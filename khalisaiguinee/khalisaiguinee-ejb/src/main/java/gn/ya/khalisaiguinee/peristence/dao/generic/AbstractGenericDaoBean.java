package gn.ya.khalisaiguinee.peristence.dao.generic;

import javax.ejb.Local;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;

import org.hibernate.Session;

/**
 * 
 * @author ydiallo
 *
 */
@Local(GenericDao.class)
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public abstract class AbstractGenericDaoBean implements GenericDao {

	protected abstract EntityManager getEm();
	
	@Override
	public <T> void create(T entity) {
		getSession().save(entity);
	}
	
	protected Session getSession() {
		return (Session) getEm().getDelegate();
	}

}
