package gn.ya.khalisaiguinee.util;

import java.io.Serializable;
import java.util.Locale;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/*
 * Author: yaya
 */
@Named("localeSelector")
@SessionScoped
public class LocalSelector implements Serializable {

	/**
	 * UID
	 */
	private static final long serialVersionUID = 1L;
	
	public void selectLanguage(String lang) {
		FacesContext context = FacesContext.getCurrentInstance();
		if(lang.equalsIgnoreCase("fr")) {
			context.getViewRoot().setLocale(Locale.FRENCH);
		}
		else {
			context.getViewRoot().setLocale(Locale.ENGLISH);
		}
	}

}
