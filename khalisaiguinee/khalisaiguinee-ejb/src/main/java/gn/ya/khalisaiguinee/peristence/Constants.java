package gn.ya.khalisaiguinee.peristence;


/**
 * 
 * @author ydiallo
 *
 */
public final class Constants {
	
	public static final String SCHEMA = "SENDMONEY";

	public final class Tables {
		
		public static final String USERS = "USERS";
		
		public static final String OPERATIONS = "OPERATIONS";
	}
	
	
}
