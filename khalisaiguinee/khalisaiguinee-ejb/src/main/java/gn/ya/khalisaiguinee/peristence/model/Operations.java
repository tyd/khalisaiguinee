package gn.ya.khalisaiguinee.peristence.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Embedded;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import gn.ya.khalisaiguinee.peristence.Constants;
import gn.ya.khalisaiguinee.peristence.model.enums.AgenciesEnum;

@Table(name = Constants.Tables.OPERATIONS, schema = Constants.SCHEMA)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "OPERATION_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class Operations extends AbstractEntity {
	
	@Embedded
	private Name name;
	
	@NotNull
	@Column(name = "AMOUNT", nullable = false)
	private BigDecimal amount;
	
	@NotBlank
	@Column(name = "CODE", nullable = false)
	private String code;
	
	@Column(name = "AGENCY", nullable = false)
	@Enumerated(EnumType.STRING)
	private AgenciesEnum agency;
	
	@Column(name = "ORANGE_MONEY")
	private boolean isOrangeMoney;
	
	@NotNull
	@NotBlank
	@Column(name = "DATE_OPERATION", nullable = false)
	private Date dateOperation;

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AgenciesEnum getAgency() {
		return agency;
	}

	public void setAgency(AgenciesEnum agency) {
		this.agency = agency;
	}

	public boolean isOrangeMoney() {
		return isOrangeMoney;
	}

	public void setOrangeMoney(boolean isOrangeMoney) {
		this.isOrangeMoney = isOrangeMoney;
	}
	
	
}
